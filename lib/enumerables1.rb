# EASY

# Define a method that returns an array of only the even numbers in its argument
# (an array of integers).
def get_evens(arr)
  arr.select {|num| num.even?}
end

# Define a method that returns a new array of all the elements in its argument
# doubled. This method should *not* modify the original array.
def calculate_doubles(arr)
  arr.map{|num| num * 2}
end

# Define a method that returns its argument with all the argument's elements
# doubled. This method should modify the original array.
def calculate_doubles!(arr)
  arr.map!{|num| num * 2}
end

# Define a method that returns the sum of each element in its argument
# multiplied by its index. array_sum_with_index([2, 9, 7]) => 23 because (2 * 0) +
# (9 * 1) + (7 * 2) = 0 + 9 + 14 = 23
def array_sum_with_index(arr)
  result = 0
  arr.each_with_index {|num,index| result += num * index}
  result
end

# MEDIUM

# Given an array of bids and an actual retail price, return the bid closest to
# the actual retail price without going over that price. Assume there is always
# at least one bid below the retail price.
def price_is_right(bids, actual_retail_price)
  lower_bids = bids.select {|bid| bid <= actual_retail_price}
  lower_bids.max
end

#using reduce
#actual_retail_price must be larger or equal to  bid
def price_is_right_reduce(bids, actual_retail_price)
  bids.reduce do |closest, bid|
    current_difference = actual_retail_price - bid
    closest_difference = actual_retail_price - closest
    if current_difference >= 0 && current_difference < closest_difference
      bid
    else
      closest
    end
  end
end

# Given an array of numbers, return an array of those numbers that have at least
# n factors (including 1 and the number itself as factors).
# at_least_n_factors([1, 3, 10, 16], 5) => [16] because 16 has five factors (1,
# 2, 4, 8, 16) and the others have fewer than five factors. Consider writing a
# helper method num_factors
def at_least_n_factors(numbers, n)
  numbers.select {|num| num_factors(num) >= n}
end

def num_factors(number)
  count = 0
  (1..number).each {|num| count+=1 if number % num == 0 }
  count
end

# HARD

# Define a method that accepts an array of words and returns an array of those
# words whose vowels appear in order. You may wish to write a helper method:
# ordered_vowel_word?
def ordered_vowel_words(words)
  words.select {|word| ordered_vowel_word?(word)}
end

#returns true if vowels are in order
def ordered_vowel_word?(word)
  #extract vowels from word, than see if it is equal to sorted
  vowels = ["a","e","i","o","u"]
  vowels_in_word = []
  word.each_char {|char| vowels_in_word << char if vowels.include?(char)}
  vowels_in_word == vowels_in_word.sort
end

# Given an array of numbers, return an array of all the products remaining when
# each element is removed from the array. You may wish to write a helper method:
# array_product.

# products_except_me([2, 3, 4]) => [12, 8, 6], where: 12 because you take out 2,
# leaving 3 * 4. 8, because you take out 3, leaving 2 * 4. 6, because you take out
# 4, leaving 2 * 3

# products_except_me([1, 2, 3, 5]) => [30, 15, 10, 6], where: 30 because you
# take out 1, leaving 2 * 3 * 5 15, because you take out 2, leaving 1 * 3 * 5
# 10, because you take out 3, leaving 1 * 2 * 5 6, because you take out 5,
# leaving 1 * 2 * 3
def products_except_me(numbers)
  total_product = array_product(numbers)
  numbers.map {|num| total_product/num}
end

def array_product(array)
  array.reduce(:*)
end
