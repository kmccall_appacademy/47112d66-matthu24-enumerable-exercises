require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr == []
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|string| string.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  frequency = Hash.new(0)
  string.each_char {|char| frequency[char] += 1 unless char == " "}
  frequency.select{|key,value| value > 1}.keys
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete("!.:;?")
  string.split.sort_by {|word| word.length}[-2..-1].reverse
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = "abcdefghijklmnopqrstuvwxyz"
  string.each_char do |char|
    letters.delete!(char)
  end
  letters.split("")
end

def missing_letters_reject(string)
  letters = ("a".."z")
  letters.reject {|char| string.include?(char)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select{|year| not_repeat_year?(year)}
end

#return true if no repeats in the year
def not_repeat_year?(year)
  year_digit_array = year.to_s.chars
  year_digit_array.uniq.length == 4
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?

#songs with no repeats are a one_week_wonder
#songs with
def one_week_wonders_reject(songs)
  repeating_songs = []
  songs.each_with_index do |song,index|
    next if index == 0
    repeating_songs << song if song == songs[index-1]
  end
  songs.reject {|song| repeating_songs.include?(song)}.uniq
end

def one_week_wonders(songs)
  songs.uniq.select {|song| no_repeats?(song,songs)}
end

#return false if a song repeats itself consecutively
#returns true for one_week_wonders
def no_repeats?(song_name, songs)
  songs.each_with_index do |song,index|
    if song == song_name
      return false if song == songs[index+1]
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

#use reduce
#remove punctuation from each word

def for_cs_sake_reduce(string)
  words_without_punctuation = remove_punctuation(string)
  #select words with "c" in them
  c_words = words_without_punctuation.split.select {|word| word.include?("c")}
  return "" if c_words == []
  c_words.reduce do |closest_c,word|
    if c_distance(word) < c_distance(closest_c)
      word
    else
      closest_c
    end
  end
end

def for_cs_sake(string)
  remove_punctuation(string)
  c_words = string.split.select{|word| word.include?("c")}
  return "" if c_words == []
  c_words.sort_by {|word|c_distance(word)}.first

end

#distance c from the end
def c_distance(word)
  word.reverse.index("c")
end

def remove_punctuation(words)
  #punctuation = "!.,?:;"
  #words.chars.reject{|char|punctuation.include?(char)}.join("")
  words.delete!("!.,?:;")
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  range = []
  arr.each_with_index do |num,index|
    if range.empty?
      range << index if num == arr[index+1]
    else
      if num != arr[index+1]
        range << index
        ranges << range
        range = []
      end
    end
  end
  ranges
end
